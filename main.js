/* 
    Task1: 

    Возьмите любую Free Rest Api, и с помощью объекта XMLHttpRequest выполните POST запрос согласно документации этого API. 

    Если есть проблема в поисках API, то можете воспользоваться этой : https://reqres.in/

    Читайте внимательно документацию самой API, дабы понять что должно отправляться при POST запросе.

    Outcome of task :

    Статус запроса: 200
    Ответ от API такой же как в ее документации

*/


const btn = document.getElementById('btn');

const createPost = (body, successRequired) => {
    const url = 'https://reqres.in/api/users';
    const xhr = new XMLHttpRequest();

    xhr.open('POST', url);

    xhr.addEventListener('load', () => {
        successRequired(JSON.parse(xhr.response));
    })
    xhr.setRequestHeader('Content-type', 'application/json;charset=UTF-8');

    xhr.send(JSON.stringify(body));
}
const successRequired = response => {
    console.log(response);
}
btn.addEventListener('click', () => {
    const newPost = {
        name: "morpheus",
        job: "leader"
    };
    createPost(newPost, successRequired);
})

/*
    Task2:

    Возьмите любую понравившуюся вам Free Rest API, и выполните Get запрос с помощью метода fetch()

    Если есть проблема в поисках API, то можете воспользоваться этой : https://reqres.in/

    Outcome of task :

    Статус запроса: 200
    Ответ от API такой же как в ее документации

*/

fetch('https://reqres.in/api/users', {
    method: 'POST',
    body: JSON.stringify({
        name: "morpheus",
        job: "leader"
    }),
    headers: {
        'Content-type': 'application/json; charset=UTF-8',
    },
})
    .then((response) => response.json())
    .then((json) => console.log(json))

/*
    Task3:

    Возьмите API с урока, с сайта jsonplaholder, и получите с сервера с помощью GET запроса комментарий с id 31. Пускай этот комментарий выводится в консоль по нажатию на кнопку которая будет у вас в html

*/

const btn2 = document.getElementById('btn2');
const requestUrl = 'https://jsonplaceholder.typicode.com/comments/';
btn2.addEventListener('click', () => {
    const getComment = id => fetch(requestUrl + id).then(response => response.json());
    getComment(31)
        .then(post => console.log(post))
        .catch(e => console.log(e));
})

/*
    Task4:

    Дан код:

    const getNumber = n => {
        return new Promise(r => {
            setTimeout(() => {
                r(n);
            },2000)
        })
    }

    const printNumber = n => {
        return new Promise(r => {
            getNumber(n).then(res => {
                r(res)
            })
        })
    }

    printNumber(100).then(data => console.log(data));


    Перепишите его используя операторы async/await
*/


const getNumber = n => {
    return new Promise(r => {
        setTimeout(() => {
            r(n);
        }, 2000)
    })
}

async function printNumber(n) {
    const result = await getNumber(n)
    console.log(result)
}
printNumber(100)




